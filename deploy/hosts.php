<?php

namespace Deployer;

//host('54.37.234.128')
//    ->user('developer')
//    ->port(1013)
//    ->forwardAgent(true)
//    ->set('composer_options', '{{composer_action}} --verbose --prefer-dist --no-progress --no-interaction --dev --optimize-autoloader')
//    ->set('deploy_path', '/var/www/bcmed');

host('54.37.234.128')
    ->user('developer')
    ->port(1013)
    ->forwardAgent(true)
    ->set('composer_options', '{{composer_action}} --verbose --prefer-dist --no-progress --no-interaction --dev --optimize-autoloader')
    ->set('deploy_path', '/var/www/bcmed');