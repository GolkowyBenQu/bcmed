<?php

namespace Deployer;

// Project name
set('application', 'bcmed.apsensa.pl');

// Project repository
set('repository', 'git@bitbucket.org:GolkowyBenQu/bcmed.git');

// [Optional] Allocate tty for git clone. Default value is false.
//set('git_tty', true);

// Additional options
set('ssh_multiplexing', false);
//set('symfony_env', 'dev');
set('keep_releases', 3);
set('var_dir', 'var');

set('bin/console', function () {
    return parse('{{bin/php}} {{release_path}}/bin/console --no-interaction');
});

//set(
//    'composer_options',
//    '{{composer_action}} --verbose --prefer-dist --no-interaction --optimize-autoloader'.
//    get('env') !== 'dev' ? '--no-dev' : ''
//);

set('env', function () {
    return [
        'APP_ENV' => 'dev',
        'MAILER_URL' => 'null://localhost',
        // Add more if you have other parameters in your .env
    ];
});