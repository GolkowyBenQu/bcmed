<?php

namespace Deployer;

task('deploy:create_cache_dir', function () {
    // Set cache dir
    set('cache_dir', '{{release_path}}/' . trim(get('var_dir'), '/') . '/cache');
    // Remove cache dir if it exist
    run('if [ -d "{{cache_dir}}" ]; then rm -rf {{cache_dir}}; fi');
    // Create cache dir
    run('mkdir -p {{cache_dir}}');
    // Set rights
    run("chmod -R g+w {{cache_dir}}");
})->desc('Create cache dir');

desc('Migrate database');
task('database:migrate', function () {
    $options = '--allow-no-migration';
    if (get('migrations_config') !== '') {
        $options = sprintf('%s --configuration={{release_path}}/{{migrations_config}}', $options);
    }
    run(sprintf('{{bin/console}} doctrine:migrations:migrate %s', $options));
});

desc('Load fixtures');
task('database:load-fixtures', function () {
    $options = '';
//    if (get('migrations_config') !== '') {
//        $options = sprintf('%s --configuration={{release_path}}/{{migrations_config}}', $options);
//    }
    run(sprintf('{{bin/console}} doctrine:fixtures:load %s', $options));
});

desc('Clear cache');
task('deploy:cache:clear', function () {
    run('{{bin/console}} cache:clear --no-warmup');
});

desc('Warm up cache');
task('deploy:cache:warmup', function () {
    run('{{bin/console}} cache:warmup');
});

desc('Create vhost');
task('deploy:vhost:create', function () {
    run('cp {{deploy_path}}/current/deploy/host.config /etc/nginx/sites-available/{{application}}');
});

desc('Disable old vhost');
task('deploy:vhost:disable', function () {
    run('rm -rf /etc/nginx/sites-enabled/{{application}}');
});

desc('Enable new vhost');
task('deploy:vhost:enable', function () {
    run('ln -s /etc/nginx/sites-available/{{application}} /etc/nginx/sites-enabled/');
});

after('deploy:vhost:create', 'deploy:vhost:disable');
after('deploy:vhost:disable', 'deploy:vhost:enable');