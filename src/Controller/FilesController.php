<?php

namespace App\Controller;

use App\Service\FileService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class FilesController extends AbstractController
{
    /**
     * @Route("/api/files", name="files_list")
     * @param FileService $fileService
     * @return JsonResponse
     */
    public function index(FileService $fileService)
    {
        $directory = '/home/bcmed/domains/bcmed.vot.pl/public_ftp';
        return new JsonResponse($fileService->getFilesTree($directory));
    }

    /**
     * @Route("/api/files/{timestamp}", name="files_by_timestamp")
     * @param string $timestamp
     * @param FileService $fileService
     * @return JsonResponse
     */
    public function getFilesByTimestamp(string $timestamp, FileService $fileService)
    {
        $directory = '/home/bcmed/domains/bcmed.vot.pl/public_ftp';
        return new JsonResponse($fileService->getFilesTree($directory, $timestamp));
    }

    /**
     * @Route("/api/download", name="download_file")
     * @param Request $request
     * @return JsonResponse
     */
    public function downloadFile(Request $request)
    {
        $requestData = json_decode($request->getContent(), true);
        $filepath = $requestData['path'];

        if (file_exists($filepath)) {
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename="'.basename($filepath).'"');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($filepath));
            readfile($filepath);
            exit;
        }

        return new JsonResponse('File not found', Response::HTTP_NOT_FOUND);
    }
}
