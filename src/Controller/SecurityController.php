<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\LoginFormType;
use App\Form\RegisterFormType;
use App\Security\LoginFormAuthenticator;
use App\Service\UserCreator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Guard\GuardAuthenticatorHandler;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController
{
    /**
     * @Route("/login", name="app_login")
     * @param AuthenticationUtils $authenticationUtils
     * @return Response
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        $form = $this->createForm(LoginFormType::class);
        $error = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render(
            'security/login.html.twig',
            [
                'form' => $form->createView(),
                'last_username' => $lastUsername,
                'error' => $error,
            ]
        );
    }

    /**
     * @Route("/logout", name="app_logout", methods={"GET"})
     */
    public function logout()
    {
        throw new \Exception('Don\'t forget to activate logout in security.yaml');
    }

    /**
     * @Route("/api", name="api_security_home")
     */
    public function apiHomeAction(): JsonResponse
    {
        $data = array(
            'message' => 'Not Authorized',
        );

        return new JsonResponse($data, Response::HTTP_FORBIDDEN);
    }

    /**
     * @Route("/api/security/authenticate", name="api_security_authenticate")
     * @param Request $request
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @return JsonResponse
     */
    public function authenticateAction(
        Request $request,
        UserPasswordEncoderInterface $passwordEncoder
    ): Response
    {
        $loginData = $request->request->all();

        /** @var User $user */
        $user = $this
            ->getDoctrine()
            ->getManager()
            ->getRepository(User::class)
            ->findOneBy([
                'email' => $loginData['email'],
            ]);

        if (!$user) {
            return new JsonResponse('Unable to find user', 404);
        }

        if ($passwordEncoder->isPasswordValid($user, $loginData['password'])) {
            return new JsonResponse([
                'id' => $user->getId(),
                'apiToken' => $user->getApiToken(),
                'firstName' => $user->getFirstName(),
                'lastName' => $user->getLastName(),
            ]);
        }

        return new JsonResponse('Unable to find user', 404);
    }
}
