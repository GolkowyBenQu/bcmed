<?php

namespace App\Controller;

use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends AbstractController
{
    /**
     * @Route("/api/user/", name="user_list")
     */
    public function index()
    {
        $em = $this->getDoctrine()->getManager();

        /** @var UserRepository $userRepo */
        $userRepo = $em->getRepository(User::class);

        /** @var User[] $users */
        $users = $userRepo->findAllAsArray();

        if (!$users) {
            throw new NotFoundHttpException();
        }

        return new JsonResponse($users);
    }

    /**
     * @Route("/api/user/{id}", name="user_details")
     * @param string $token
     * @return JsonResponse
     */
    public function userDetails(int $id)
    {
        $em = $this->getDoctrine()->getManager();

        /** @var UserRepository $userRepo */
        $userRepo = $em->getRepository(User::class);

        /** @var User $user */
        $user = $userRepo->findOneById($id);

        if (!$user) {
            throw new NotFoundHttpException();
        }

        return new JsonResponse($user);
    }
}
