<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture
{
    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager)
    {
        $user = new User();
        $user->setEmail('user@gmail.com');
        $user->setPassword($this->passwordEncoder->encodePassword($user, 'password'));
        $user->setApiToken(1234);
        $user->setFirstName('UserName');
        $user->setLastName('UserSurname');
        $manager->persist($user);

        $user = new User();
        $user->setEmail('admin@gmail.com');
        $user->setPassword($this->passwordEncoder->encodePassword($user, 'password'));
        $user->setRoles(['ROLE_ADMIN']);
        $user->setApiToken(1234);
        $user->setFirstName('AdminName');
        $user->setLastName('AdminSurname');
        $manager->persist($user);

        $manager->flush();
    }
}
