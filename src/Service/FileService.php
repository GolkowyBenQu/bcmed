<?php

namespace App\Service;

class FileService
{
    /**
     * @param string $directory
     * @param string $timestamp
     * @return array
     */
    public function getFilesTree(string $directory, string $timestamp = '')
    {
        $shortPath = '';

        return $this->getFiles($directory, $shortPath, $timestamp);
    }

    /**
     * @param string $directory
     * @param string $shortPath
     * @param string $timestamp
     * @return array
     */
    private function getFiles(string $directory, string $shortPath = '', string $timestamp = '')
    {
        $scanned_directory = array_diff(scandir($directory), array('..', '.'));
        $files = [];

        foreach ($scanned_directory as $file) {
            if (!is_file($directory . '/' . $file)) {
                $files = array_merge($files, $this->getFiles(
                    $directory . '/' . $file,
                    $shortPath . '/' . $file,
                    $timestamp
                ));
            } else {
                $fileTimestamp = filemtime($directory . '/' . $file);
                if (!$timestamp || ($timestamp && $fileTimestamp >= $timestamp)) {
                    $files[]  = [
                        'fullPath' => $directory . '/' . $file,
                        'shortPath' => $shortPath . '/' . $file,
                        'file' => $file,
                        'timestamp' => $fileTimestamp,
                    ];
                }
            }

        }

        return $files;
    }
}
